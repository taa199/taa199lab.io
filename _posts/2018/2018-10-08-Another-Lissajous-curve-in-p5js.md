---
layout: post
title:	"Another Lissajous curve in p5.js"
date:	2018-10-08 01:00:00
categories:
    - posts
tags:
    - processing
    - p5js
---
<div id="p5-container" style="margin-left: 180px;">
</div>
<script type="text/javascript" src="{{ site.baseurl }}js/lib/p5.min.js"></script>
<script type="text/javascript" src="{{ site.baseurl }}js/sketches/lissajous/sketch.js"></script>
<script type="text/javascript" src="{{ site.baseurl }}js/sketches/lissajous/curve.js"></script>
<script>
</script>